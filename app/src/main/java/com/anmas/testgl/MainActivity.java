package com.anmas.testgl;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initGridView();
    }

    private void initGridView() {
        ExcelGridView grid2 = (ExcelGridView) findViewById(R.id.grid);
        grid2.setAdapter(new GrAdapter(this));
    }

    private class GrAdapter extends BaseAdapter {

        private final int ITEMS_NUM = 1000000;

        private final String[] array;
        private Context context;


        public GrAdapter(Context context) {
            this.context = context;
            array = new String[ITEMS_NUM];
            for (int i = 0; i < array.length; i++) {
                array[i] = String.valueOf(i);
            }

        }

        @Override
        public int getCount() {
            return array.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                VH vh = new VH();
                convertView = LayoutInflater.from(context).inflate(R.layout.grid_item, parent, false);
                vh.tv = (TextView) convertView.findViewById(android.R.id.text1);
                convertView.setTag(vh);
            }
            VH vh = (VH) convertView.getTag();
            vh.tv.setText(array[position]);
            return convertView;
        }

        private class VH {
            TextView tv;
        }
    }
}
