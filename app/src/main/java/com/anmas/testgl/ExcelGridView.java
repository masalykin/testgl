package com.anmas.testgl;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Scroller;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Slow and bad
 */
public class ExcelGridView extends AdapterView<BaseAdapter> {

    private static final int LAYOUT_MODE_BELOW = -1;

    private final static int CAPTION_VIEW_RES_ID = R.layout.column_item;

    private final GestureDetector gestureDetector;
    private final Scroller scroller;
    private final ReusableCache reusableCache;

    private BaseAdapter adapter;

    private int rowsCount;
    private int columnsCount;

    private int listLeft;
    private int listTop;
    private int prevCurrY;
    private int prevCurrX;

    private int firstVisibleRow;
    private int firstVisibleColumn;
    private int lastVisibleRow;
    private int lastVisibleColumn;

    private int maxColumnWidth;
    private int maxRowHeight;

    private int maxCaptionViewWidth;
    private int maxCaptionViewHeight;

    private int itemViewsCount;
    private int lastChildRowView;
    private int lastChildColumnView;

    public ExcelGridView(Context context) {
        this(context, null);
    }

    public ExcelGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        reusableCache = new ReusableCache();
        scroller = new Scroller(getContext());
        gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                scrollIfAllowed((int) distanceX, (int) distanceY);
                return true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                scroller.forceFinished(true);

                int startX = (int) e1.getX();
                int startY = (int) e1.getY();

                prevCurrX = startX;
                prevCurrY = startY;

                scroller.fling(
                        startX,
                        startY,
                        -(int) velocityX,
                        -(int) velocityY,
                        Integer.MIN_VALUE, Integer.MAX_VALUE,
                        Integer.MIN_VALUE, Integer.MAX_VALUE
                );
//                ViewCompat.postInvalidateOnAnimation(ExcelGridView.this);
                return true;
            }
        });
    }

    private void calcRowsColumns(int childCount) {
        columnsCount = (int) Math.round(Math.sqrt(childCount));
        rowsCount = (int) Math.ceil((double) childCount / columnsCount);
        lastVisibleColumn = columnsCount;
    }

    private int getItemPosition(int row, int column) {
        return row * columnsCount + column;
    }

    @Override
    public BaseAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void setAdapter(BaseAdapter adapter) {
        this.adapter = adapter;
        calcRowsColumns(adapter.getCount());
        removeAllViewsInLayout();
        requestLayout();
    }

    @Override
    public void computeScroll() {
        super.computeScroll();

        if (scroller.computeScrollOffset()) {
            int deltaX = scroller.getCurrX() - prevCurrX;
            int deltaY = scroller.getCurrY() - prevCurrY;

            scrollIfAllowed(deltaX, deltaY);

            prevCurrX = scroller.getCurrX();
            prevCurrY = scroller.getCurrY();

        }
    }

    @Override
    public View getSelectedView() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public void setSelection(int position) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return gestureDetector.onTouchEvent(ev);
    }

    private void scrollIfAllowed(int distanceX, int distanceY) {

        listLeft -= distanceX;
        listTop -= distanceY;

        if (firstVisibleRow == 0 && listTop > 0)
            listTop = 0;
        if (firstVisibleColumn == 0 && listLeft > 0)
            listLeft = 0;

        requestLayout();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (adapter == null || adapter.isEmpty())
            return;

        if (getChildCount() > 0)
            updateVisibleBounds();
        else
            firstFillGrid();

        addCaptions();

        positionItems();
        invalidate();

    }

    private void updateVisibleBounds() {
        View child = getChildAt(0);
        if (child != null) {
            while (firstVisibleRow > 0 && listTop + maxRowHeight > getTop()) {
                firstVisibleRow--;
                addRow(firstVisibleRow);
                listTop -= child.getHeight();
            }
            if (firstVisibleRow < rowsCount - 1 && child.getBottom() + maxRowHeight < getTop()) {
                removeRow(firstVisibleRow);
                firstVisibleRow++;
                listTop += child.getHeight();
            }

            while (firstVisibleColumn > 0 && listLeft + maxColumnWidth > getLeft()) {
                firstVisibleColumn--;
                addColumn(firstVisibleColumn);
                listLeft -= child.getWidth();
            }
            if (firstVisibleColumn < columnsCount - 1 && child.getRight() + maxColumnWidth < getLeft()) {
                removeColumn(firstVisibleColumn);
                firstVisibleColumn++;
                listLeft += child.getWidth();
            }
        }
        child = getChildAt(getVisibleChildPosition(lastVisibleRow - firstVisibleRow, 0));
        if (child != null) {
            if (lastVisibleRow < rowsCount - 1 && child.getBottom() - maxRowHeight < getBottom()) {
                lastVisibleRow++;
                addRow(lastVisibleRow);
            }
            if (lastVisibleRow > 0 && child.getTop() - maxRowHeight > getBottom()) {
                removeRow(lastVisibleRow);
                lastVisibleRow--;
            }
        }

        child = getChildAt(getVisibleChildPosition(0, lastVisibleColumn - firstVisibleColumn));
        if (child != null) {
            if (lastVisibleColumn < columnsCount - 1 && child.getRight() - maxColumnWidth < getRight()) {
                lastVisibleColumn++;
                addColumn(lastVisibleColumn);
            }
            if (lastVisibleColumn > 0 && child.getLeft() - maxColumnWidth > getRight()) {
                removeColumn(lastVisibleColumn);
                lastVisibleColumn--;
            }
        }
    }

    private int getVisibleColumnsCount() {
        return lastVisibleColumn - firstVisibleColumn + 1;
    }

    private int getVisibleRowsCount() {
        return lastVisibleRow - firstVisibleRow + 1;
    }

    private int getVisibleChildPosition(int visibleRowIndex, int visibleColumnIndex) {
        return visibleRowIndex * getVisibleColumnsCount() + visibleColumnIndex;
    }

    private void addRow(final int rowIndex) {
        int index = getVisibleChildPosition(rowIndex - firstVisibleRow, 0);
        for (int columnIndex = firstVisibleColumn; columnIndex < lastVisibleColumn + 1; columnIndex++) {
            int position = getItemPosition(rowIndex, columnIndex);

            if (position < adapter.getCount()) {

                View child = getNewItemView(position, reusableCache.get(ChildType.Item));

                addItemView(child, index);
                index++;
                itemViewsCount++;

            }
        }
    }

    private void removeRow(final int rowIndex) {
        final int visibleColumnsCount = getVisibleColumnsCount();
        final int childIndex = getVisibleChildPosition(rowIndex - firstVisibleRow, 0);
        for (int columnIndex = 0; columnIndex < visibleColumnsCount; columnIndex++) {
            View child = getChildAt(childIndex);
            removeViewInLayout(child);
            reusableCache.put(ChildType.Item, child);
            itemViewsCount--;
        }
    }

    private void addColumn(final int columnIndex) {
        for (int rowIndex = firstVisibleRow; rowIndex < lastVisibleRow + 1; rowIndex++) {
            int position = getItemPosition(rowIndex, columnIndex);
            int childIndex = getVisibleChildPosition(rowIndex - firstVisibleRow, columnIndex - firstVisibleColumn);

            if (position < adapter.getCount()) {

                View child = getNewItemView(position, reusableCache.get(ChildType.Item));

                addItemView(child, childIndex);
                itemViewsCount++;
            }
        }
    }

    private void removeColumn(final int columnIndex) {
        final int visibleRowsCount = getVisibleRowsCount();
        for (int rowIndex = 0; rowIndex < visibleRowsCount; rowIndex++) {
            int childIndex = getVisibleChildPosition(rowIndex, columnIndex - firstVisibleColumn) - rowIndex;

            View child = getChildAt(childIndex);
            removeViewInLayout(child);
            reusableCache.put(ChildType.Item, child);
            itemViewsCount--;
        }
    }

    private void firstFillGrid() {
        int topEdge = 0;
        int lvc = 0;

        itemViewsCount = 0;
        for (int row = firstVisibleRow; row < rowsCount && topEdge < getHeight(); row++) {
            int rowHeight = 0;

            for (int columnIndex = firstVisibleColumn, leftEdge = 0; leftEdge < getWidth() && columnIndex < lastVisibleColumn; columnIndex++) {
                int position = getItemPosition(row, columnIndex);

                if (position < adapter.getCount()) {

                    View child = getNewItemView(position, reusableCache.get(ChildType.Item));

                    addItemView(child);

                    itemViewsCount++;

                    leftEdge += child.getMeasuredWidth();

                    if (child.getMeasuredHeight() > rowHeight)
                        rowHeight = child.getMeasuredHeight();

                    if (columnIndex > lvc)
                        lvc = columnIndex;


                } else
                    break;
            }
            lastVisibleRow = row;
            topEdge += rowHeight;
        }

        if (lvc != lastVisibleColumn)
            lastVisibleColumn = lvc;
    }

    private View getNewItemView(int position, View convertView) {
        View child = adapter.getView(position, convertView, this);
        child.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);

        int childWidth = child.getMeasuredWidth();
        int childHeight = child.getMeasuredHeight();

        if (childWidth > maxColumnWidth)
            maxColumnWidth = childWidth;

        if (childHeight > maxRowHeight)
            maxRowHeight = childHeight;

        return child;
    }

    private void addItemView(View child) {
        addItemView(child, LAYOUT_MODE_BELOW);
    }

    private void addItemView(View child, int index) {
        LayoutParams params = child.getLayoutParams();
        if (params == null) {
            params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        }
        addViewInLayout(child, index, params, true);

    }

    private void addCaptions() {

        while (itemViewsCount < getChildCount()) {
            View captionView = getChildAt(itemViewsCount);
            removeViewInLayout(captionView);
            reusableCache.put(ChildType.Caption, captionView);
        }

        lastChildRowView = itemViewsCount;

        for (int row = firstVisibleRow; row < lastVisibleRow + 1; row++) {
            addCaptionView("" + row);
            lastChildRowView++;
        }
        lastChildColumnView = lastChildRowView;
        for (int column = firstVisibleColumn; column < lastVisibleColumn + 1; column++) {
            addCaptionView("" + column);
            lastChildColumnView++;
        }
    }

    private void addCaptionView(String text) {
        View captionView = reusableCache.get(ChildType.Caption);
        if (captionView == null)
            captionView = LayoutInflater.from(getContext()).inflate(CAPTION_VIEW_RES_ID, this, false);

        ((TextView) captionView.findViewById(android.R.id.text1)).setText(text);

        captionView.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);

        if (captionView.getMeasuredWidth() > maxCaptionViewWidth)
            maxCaptionViewWidth = captionView.getMeasuredWidth();
        if (captionView.getMeasuredHeight() > maxCaptionViewHeight)
            maxCaptionViewHeight = captionView.getMeasuredHeight();

        LayoutParams params = captionView.getLayoutParams();
        if (params == null) {
            params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        }
        addViewInLayout(captionView, LAYOUT_MODE_BELOW, params, true);
    }

    private void positionItems() {

        if (maxColumnWidth < maxCaptionViewWidth)
            maxColumnWidth = maxCaptionViewWidth;
        if (maxRowHeight < maxCaptionViewHeight)
            maxRowHeight = maxCaptionViewHeight;

        //layout rows captions
        for (int childIndex = itemViewsCount, top = maxCaptionViewHeight + listTop, left = getLeft(); childIndex < lastChildRowView; childIndex++) {
            View child = getChildAt(childIndex);
            child.layout(left, top, left + maxCaptionViewWidth, top + maxRowHeight);
            top += maxRowHeight;
        }

        //layout columns captions
        for (int childIndex = lastChildRowView, top = getTop(), left = maxCaptionViewWidth + listLeft; childIndex < lastChildColumnView; childIndex++) {
            View child = getChildAt(childIndex);
            child.layout(left, top, left + maxColumnWidth, top + maxCaptionViewHeight);
            left += maxColumnWidth;
        }
        //layout items
        int top = listTop + maxCaptionViewHeight;
        int left = listLeft + maxCaptionViewWidth;
        for (int childIndex = 0, column = firstVisibleColumn; childIndex < itemViewsCount; childIndex++) {
            View child = getChildAt(childIndex);

            child.layout(left, top, left + maxColumnWidth, top + maxRowHeight);
            left += maxColumnWidth;

            column++;

            if (column == lastVisibleColumn + 1) {
                column = firstVisibleColumn;
                left = listLeft + maxCaptionViewWidth;
                top += maxRowHeight;
            }
        }
    }

    private enum ChildType {
        Item,
        Caption
    }

    private static class CachedView {
        View view;
        ChildType type;

        public CachedView(ChildType type, View view) {
            this.type = type;
            this.view = view;
        }
    }

    private class ReusableCache {
        private List<CachedView> cache = new ArrayList<>();

        private void put(ChildType type, View view) {
            cache.add(new CachedView(type, view));
        }

        private View get(ChildType type) {
            for (CachedView cachedView : cache) {
                if (type == cachedView.type) {
                    cache.remove(cachedView);
                    return cachedView.view;
                }
            }
            return null;
        }

        private int size() {
            return cache.size();
        }
    }

}
